﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public class PartnerBuilder
    {
        private Partner _partner;
        public PartnerBuilder()
        {
            _partner = new Partner
            {
                PartnerLimits = new List<PartnerPromoCodeLimit>()
            };
        }

        public PartnerBuilder(Guid id)
        {
            _partner = new Partner { Id = id };
        }

        public PartnerBuilder WithName(string name)
        {
            _partner.Name = name;
            return this;
        }

        public PartnerBuilder WithNumberIssuedPromoCodes(int number)
        {
            _partner.NumberIssuedPromoCodes = number;
            return this;
        }

        public PartnerBuilder WhichIsActive()
        {
            _partner.IsActive = true;
            return this;
        }

        public PartnerBuilder WhichIsNotActive()
        {
            _partner.IsActive = false;
            return this;
        }

        public Partner Build() => _partner;

        public PartnerBuilder WithLimit(PartnerPromoCodeLimit limit)
        {
            _partner.PartnerLimits = new List<PartnerPromoCodeLimit>() { limit };
            return this;
        }
    }
}
