﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public class SetPromoCodeLimitRequestBuilder
    {
        private int _limit;
        private DateTime _endDate;

        public static SetPromoCodeLimitRequestBuilder Create() => new SetPromoCodeLimitRequestBuilder();

        public SetPromoCodeLimitRequestBuilder WithLimit(int limit)
        {
            _limit = limit;
            return this;
        }

        public SetPromoCodeLimitRequestBuilder WithEndDate(DateTime endDate)
        {
            _endDate = endDate;
            return this;
        }

        public SetPartnerPromoCodeLimitRequest Build()
        {
            return new SetPartnerPromoCodeLimitRequest
            {
                Limit = _limit,
                EndDate = _endDate
            };
        }
    }
}
