﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.WebHost;

namespace Otus.Teaching.PromoCodeFactory.UnitTests
{
    public static class Configuration
    {
        public static IServiceCollection GetServiceCollection(IConfigurationRoot configuration, IServiceCollection serviceCollection = null)
        {
            if (serviceCollection == null)
            {
                serviceCollection = new ServiceCollection();
            }
            serviceCollection
                .AddSingleton(configuration)
                .AddSingleton((IConfiguration)configuration)
                .ConfigureRepositories()
                .AddMemoryCache();
            return serviceCollection;
        }

        public static IServiceCollection ConfigureInMemoryContext(this IServiceCollection services)
        {
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();
            services.ConfigureContext(options =>
            {
                options.UseInMemoryDatabase("InMemoryDb");
                options.UseInternalServiceProvider(serviceProvider);
            });
            return services;
        }
    }
}
