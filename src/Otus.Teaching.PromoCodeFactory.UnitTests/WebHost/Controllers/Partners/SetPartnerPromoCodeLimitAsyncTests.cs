﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.UnitTests.Builders;
using Otus.Teaching.PromoCodeFactory.UnitTests.Fixtures;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests : IClassFixture<InMemoryDBFixture>
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public SetPartnerPromoCodeLimitAsyncTests(InMemoryDBFixture inMemoryDBFixture)
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();

            _serviceProvider = inMemoryDBFixture.ServiceProvider;
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Empty;
            var validLimit = CreateValidLimitRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync((Partner)null);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, validLimit);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partner = CreateBasePartnerBuilder()
                .WhichIsNotActive()
                .Build();
            var limitRequest = CreateValidLimitRequest();
            var requestedPartnerId = partner.Id;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(requestedPartnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(requestedPartnerId, limitRequest);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsNotPositive_ReturnsBadRequest(int limit)
        {
            // Arrange
            var limitRequest = SetPromoCodeLimitRequestBuilder.Create()
                .WithLimit(limit)
                .Build();
            var partner = CreateBasePartnerBuilder()
                .WhichIsActive()
                .Build();
            
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, limitRequest);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        [Theory]
        [InlineData(1, 1000, 0)]
        [InlineData(-1, 1000, 1000)]
        public async void SetPartnerPromoCodeLimitAsync_LimitWasUpdate_PromocodesAmountShouldBeResetForActualLimitOnly(int limitExceedInDays, int initialPromoCodesAmount, int expectedPromocodesAmount)
        {
            // Arrange
            var partner = CreateBasePartnerBuilder()
                .WhichIsActive()
                .WithNumberIssuedPromoCodes(initialPromoCodesAmount)
                .Build();
            var limitRequest = SetPromoCodeLimitRequestBuilder.Create()
                .WithLimit(1)
                .WithEndDate(DateTime.Now.AddDays(limitExceedInDays))
                .Build();
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, limitRequest);
            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.NumberIssuedPromoCodes.Should().Be(expectedPromocodesAmount);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitWasSet_PreviousLimitShouldBeCancelled()
        {
            // Arrange            
            var partner = CreateBasePartnerBuilder()
                .WhichIsActive()
                .WithLimit(new PartnerPromoCodeLimit())
                .Build();
            var limitRequest = CreateValidLimitRequest();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, limitRequest);
            
            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.PartnerLimits.First().CancelDate.Should().BeBefore(DateTime.Now);
        }

        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitWasSet_LimitShouldBeSavedToDB()
        {
            // Arrange
            var inMemoryRepository = _serviceProvider.GetRequiredService<IRepository<Partner>>();
            var partnersController = new PartnersController(inMemoryRepository);
            var partner = FakeDataFactory.Partners.First();
            var initialPartnersLimitsAmount = partner.PartnerLimits.Count();
            var limitRequest = CreateValidLimitRequest();

            // Act
            var result = await partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, limitRequest);
            partner = await inMemoryRepository.GetByIdAsync(partner.Id);
            // Assert
            result.Should().BeAssignableTo<CreatedAtActionResult>();
            
            partner.PartnerLimits.Count().Should().Be(initialPartnersLimitsAmount + 1);
            partner.PartnerLimits.Last().Limit.Should().Be(limitRequest.Limit);
        }

        private SetPartnerPromoCodeLimitRequest CreateValidLimitRequest() => SetPromoCodeLimitRequestBuilder
            .Create()
            .WithLimit(1)
            .WithEndDate(DateTime.Now.AddDays(3))
            .Build();

        private PartnerBuilder CreateBasePartnerBuilder()
        {
            var limit = new PartnerPromoCodeLimit()
            {
                Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                CreateDate = new DateTime(2020, 07, 9),
                EndDate = new DateTime(2020, 10, 9),
                Limit = 100
            };
            var partner = new PartnerBuilder(Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"))
                .WithName("Суперигрушки")
                .WithLimit(limit);

            return partner;
        }
    }
}