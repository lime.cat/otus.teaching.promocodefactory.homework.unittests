﻿namespace Otus.Teaching.PromoCodeFactory.Core.Validation
{
    public static class ValidationMessages
    {
        public const string PartnerNotActive = "Данный партнер не активен";
        public const string InvalidLimitRequested = "Лимит должен быть больше 0";
    }
}
