﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapping
{
    public static class PromoCodeLimitsMapper
    {
        public static PartnerPromoCodeLimitResponse MapToResponseModel(this PartnerPromoCodeLimit limit)
        {
            return new PartnerPromoCodeLimitResponse()
            {
                Id = limit.Id,
                PartnerId = limit.PartnerId,
                Limit = limit.Limit,
                CreateDate = limit.CreateDate.ToString(Constants.DefaultDateTimeFormat),
                EndDate = limit.EndDate.ToString(Constants.DefaultDateTimeFormat),
                CancelDate = limit.CancelDate?.ToString(Constants.DefaultDateTimeFormat),
            };
        }
    }
}
