﻿namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public static class Constants
    {
        public const string DefaultDateTimeFormat = "dd.MM.yyyy hh:mm:ss";
    }
}
