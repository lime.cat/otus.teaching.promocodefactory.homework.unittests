﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public static class Configuration
    {
        public static IServiceCollection ConfigureRepositories(this IServiceCollection services) => services
            .AddScoped(typeof(IRepository<>), typeof(EfRepository<>))
            .AddScoped<IDbInitializer, EfDbInitializer>();

        public static IServiceCollection ConfigureContext(this IServiceCollection services, Action<DbContextOptionsBuilder> optionsBuilder)
        {
            services.AddDbContext<DataContext>(optionsBuilder);
            return services;
        }
    }
}
